﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Hotel.Models
{
    public class Branch
    {
        public int ID { get; set; }
        public string Address { get; set; }
        public IEnumerable<Room> Rooms { get; set; }

    }
}