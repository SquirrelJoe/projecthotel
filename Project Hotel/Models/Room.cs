﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_Hotel.Models
{
    public class Room
    {
        public int ID { get; set; }
        public string ReferenceNumber { get; set; }
        public string RoomNumber { get; set; }
        public double Price { get; set; }
        public string RoomType { get; set; }
        public double CostPerNight { get; set; }
        public bool Booked { get; set; }
        public int CurrentOccupentID { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BookingStartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BookingEndDate { get; set; }
        public int BedCount { get; set; }
        public bool OrderedBreakfast { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public IEnumerable<Component> Components { get; set; }
        public Customers Customer { get; set; }

    }
}