﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Hotel.Models
{
    public class RoomType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PersonAmount { get; set; }

    }
}