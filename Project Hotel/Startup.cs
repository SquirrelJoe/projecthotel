﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Project_Hotel.Startup))]
namespace Project_Hotel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
