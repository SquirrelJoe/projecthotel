// <auto-generated />
namespace Project_Hotel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddNumberofpeopleAndOrderBreakfast : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddNumberofpeopleAndOrderBreakfast));
        
        string IMigrationMetadata.Id
        {
            get { return "201812032210404_AddNumberofpeopleAndOrderBreakfast"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
