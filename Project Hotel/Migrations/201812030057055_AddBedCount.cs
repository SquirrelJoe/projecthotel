namespace Project_Hotel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBedCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rooms", "BedCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rooms", "BedCount");
        }
    }
}
