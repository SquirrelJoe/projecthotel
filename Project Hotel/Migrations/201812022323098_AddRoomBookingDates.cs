namespace Project_Hotel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoomBookingDates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rooms", "BookingStartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Rooms", "BookingEndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rooms", "BookingEndDate");
            DropColumn("dbo.Rooms", "BookingStartDate");
        }
    }
}
