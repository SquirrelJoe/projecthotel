namespace Project_Hotel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rooms : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ReferenceNumber = c.String(),
                        RoomNumber = c.String(),
                        Price = c.Double(nullable: false),
                        RoomType = c.String(),
                        CostPerNight = c.Double(nullable: false),
                        Booked = c.Boolean(nullable: false),
                        CurrentOccupentID = c.Int(nullable: false),
                        Customer_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.Customer_ID)
                .Index(t => t.Customer_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "Customer_ID", "dbo.Customers");
            DropIndex("dbo.Rooms", new[] { "Customer_ID" });
            DropTable("dbo.Rooms");
        }
    }
}
