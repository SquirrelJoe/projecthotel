namespace Project_Hotel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateTheDatabase : DbMigration
    {
        public override void Up()
        {
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Fred', '', 'Blogs', 'FredBlogs@Email.com', '123 Fake Street', '09/09/2000', '0274589651')");
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Jamie', 'Berg', 'Anderson', 'JamieAnderson@Email.com', '124 Fake Street', '08/08/2000', '0214588656')");
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Nani', 'Wu Du', 'Deska', 'NaniDeska@Email.com', '125 Fake Street', '07/07/2000', '0274589651')");
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Ashley', '', 'Perry', 'AshleyPerry@Email.com', '126 Fake Street', '06/06/2000', '0275986318')");
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Hank', '', 'Roberts', 'HankRoberts@Email.com', '127 Fake Street', '05/05/2000', '0216487521')");
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Jacob', '', 'Hanson', 'JacobHanson@Email.com', '128 Fake Street', '04/04/2000', '0217895663')");
            Sql("Insert into Customers (Firstname, MiddleName, Lastname, Email, Address, DOB, Phone) Values ('Daniel', '', 'Nobel', 'DanielNobel@Email.com', '129 Fake Street', '03/03/2000', '0271548695')");
            Sql("Insert into Rooms (RoomNumber, Price, RoomType, CostPerNight, Booked, CurrentOccupentID) Values ('1', '50', 'Single', '100', 'False', '0')");
            Sql("Insert into Rooms (RoomNumber, Price, RoomType, CostPerNight, Booked, CurrentOccupentID) Values ('2', '100', 'King Single', '125', 'False', '0')");
            Sql("Insert into Rooms (RoomNumber, Price, RoomType, CostPerNight, Booked, CurrentOccupentID) Values ('3', '125', 'Double', '150', 'False', '0')");
            Sql("Insert into Rooms (RoomNumber, Price, RoomType, CostPerNight, Booked, CurrentOccupentID) Values ('4', '150', 'Queen', '175', 'False', '0')");
            Sql("Insert into Rooms (RoomNumber, Price, RoomType, CostPerNight, Booked, CurrentOccupentID) Values ('5', '200', 'King', '200', 'False', '0')");
        }
        
        public override void Down()
        {
            Sql("Delete From Customers");
            Sql("DBCC CHECKIDENT(Customers, RESEED,0);");
            Sql("Delete From Rooms");
            Sql("DBCC CHECKIDENT(Rooms, RESEED,0);");
        }
    }
}
