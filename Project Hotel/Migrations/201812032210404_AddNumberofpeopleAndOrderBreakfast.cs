namespace Project_Hotel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNumberofpeopleAndOrderBreakfast : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rooms", "OrderedBreakfast", c => c.Boolean(nullable: false));
            AddColumn("dbo.Rooms", "NumberOfAdults", c => c.Int(nullable: false));
            AddColumn("dbo.Rooms", "NumberOfChildren", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rooms", "NumberOfChildren");
            DropColumn("dbo.Rooms", "NumberOfAdults");
            DropColumn("dbo.Rooms", "OrderedBreakfast");
        }
    }
}
