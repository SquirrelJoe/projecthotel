﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Project_Hotel.Models;

namespace Project_Hotel.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {

                var Customers = Context.Customers.ToList();
                return View(Customers);
            }
        }


        public ActionResult Single(int ID)
        {
            Customers Customers;
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                Customers = Context.Customers.Find(ID);
                if (Customers == null)
                {
                    return HttpNotFound();
                }

                return View(Customers);
            }
        }

        public ActionResult Edit(int ID)
        {
            Customers Customers;
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                Customers = Context.Customers.Find(ID);
                if (Customers == null)
                {
                    return HttpNotFound();
                }

                return View(Customers);
            }
        }

        public ActionResult Save(Customers Customers)
        {
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                if (Customers.ID == 0)
                {
                    Context.Customers.Add(Customers);
                }
                else
                {

                    TryUpdateModel(Customers);
                    Context.Entry(Customers).State = EntityState.Modified;
                }
                Context.SaveChanges();
                return RedirectToAction("Index", "Customer");
            }
        }
    }
}