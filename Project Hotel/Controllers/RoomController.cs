﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Project_Hotel.Models;

namespace Project_Hotel.Controllers
{
    public class RoomController : Controller
    {
        // GET: Room
        public ActionResult Index()
        {
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {

                var Rooms = Context.Rooms.ToList();
                return View(Rooms);
            }
        }

        public ActionResult Single(int ID)
        {
            Room room;
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                room = Context.Rooms.Find(ID);
                if (room == null)
                {
                    return HttpNotFound();
                }

                return View(room);
            }
        }

        public ActionResult Edit(int ID)
        {
            Room room;
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                room = Context.Rooms.Find(ID);
                if (room == null)
                {
                    return HttpNotFound();
                }

                return View(room);
            }
        }

        public ActionResult Save(Room Room)
        {
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                if (Room.ID == 0)
                {
                    Context.Rooms.Add(Room);
                }
                else
                {
                    //var customerInDB = _context.Customers.Single(c => c.ID == customer.ID);
                    TryUpdateModel(Room);
                    Context.Entry(Room).State = EntityState.Modified;
                }
                Context.SaveChanges();
                return RedirectToAction("Index", "Room");
            }
        }

        public ActionResult Book(int ID)
        {
            Room room;
            using (ApplicationDbContext Context = new ApplicationDbContext())
            {
                room = Context.Rooms.Find(ID);
                if (room == null)
                {
                    return HttpNotFound();
                }

                return View(room);
            }

        }


    }
}
